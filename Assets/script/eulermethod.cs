﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eulermethod: MonoBehaviour
{
    private const float g = 9.81f;
    public float mass=3;
    private Vector3 Pos;
    private Vector3 vel;
    private Vector3 acc;
    private float airresist=1;
  //  private float v=0f;
    // Start is called before the first frame update
    void Start()
    {
        vel = Vector3.zero;
        //a=F/M = (W-D)/m= (m*g-d)/mass
        acc = new Vector3(0f, -g, 0f)*mass- new Vector3(0f, airresist, 0f) / mass;
     
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        vel = vel + acc * Time.fixedDeltaTime;
        Pos = Pos+ vel*Time.fixedDeltaTime;
        transform.position = Pos;



        /* float deltaT = Time.fixedDeltaTime;
         float delta = (v * deltaT) + -g * deltaT * deltaT * 0.5f;
         v =+ -g * deltaT;
        vel += acc * deltaT;
         Pos.y +=   delta;
         transform.position = Pos;
         Debug.Log(Pos);*/
    }
}
