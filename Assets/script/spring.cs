﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spring: MonoBehaviour
{
    private const float g = 9.81f;
    public float mass=30;
    private float Pos;
    private float vel;
    private float Force;
    private float acceleration;
    public float springforce;
    public Transform anchor;
    public float stifness = 7;
    public float damping = 3;
    public float dampingforce;
    public float lr = 3;
  //  private float v=0f;
    // Start is called before the first frame update
    void Start()
    {
        vel =transform.position.y;
      
        
        //acceleration=F/M
      
        //spring Force
   }

    // Update is called once per frame
    void FixedUpdate()
    {
        float lc = Vector3.Distance(transform.position, anchor.position);
        Vector3 difference = transform.position - anchor.position;
        Vector3 unitV = difference / difference.magnitude;
        Vector3 Fs = -stifness * (lc - lr) * unitV;


        springforce = Fs.y;
        dampingforce = damping * vel;
        Force = springforce + -g * mass-dampingforce;
        //acceleration=F/M
        acceleration = Force / mass;
        vel = vel + acceleration * Time.fixedDeltaTime;
        Pos = Pos+ vel*Time.fixedDeltaTime;
        transform.position =new Vector3(transform.position.x ,Pos,0);

    }
}
