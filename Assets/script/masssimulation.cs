﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class masssimulation : MonoBehaviour
{
    private const float g = 9.81f;
    public float mass = 30;
    public Vector3 pos;
    public Vector3 vel;
    public Vector3 force;
    Vector3 dampingforce;
    public Vector3 acceleration;
    public Vector3 springforce;
    public Vector3 SpringForce;
    public Transform top;
    public Transform top2;
    public Transform topLeft;
    public Transform topright;
    public Transform downLeft;
    public Transform downright;
    public Transform left;
    public Transform right;
    public Transform right2;
    public Transform left2;
    public Transform down;
    public Transform down2;
    public float elacticity;
    public float taille = 0;
    public float Length = 0;
    public float damping = 3;
    public List<Transform> springlist = new List<Transform>();
    float index;
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
        taille = Mathf.Sqrt(springlist.Count);
        index = springlist.IndexOf(this.transform);
        springforce = Vector3.zero;
        acceleration = Vector3.zero;
        SpringForce = Vector3.zero;
        int k = 0;
        for (int i = 1; i <= taille; i++)
        {
            for (int j = 1; j <= taille; j++)
            {
                k++;
                if (k == (int)index + 1)
                {
                    if (i != 1 && j != 1)
                    {
                        {
                            left = springlist[(int)index - 1];
                            topLeft = springlist[(int)index - (int)taille - 1];
                        }

                    }
                    if (i > 1)
                    {
                        top = springlist[(int)index - (int)taille];
                    }
                    if (i != taille && j != 1)
                    {
                        downLeft = springlist[(int)index + (int)taille - 1];
                    }
                    if (i != taille && j != taille)
                    {
                        downright = springlist[(int)index + (int)taille + 1];
                    }
                    if (i != 1 && j != taille)
                    {
                        right = springlist[(int)index + 1];
                        topright = springlist[(int)index - (int)taille + 1];
                    }
                    if (j + 2 <= taille)
                    {
                        right2 = springlist[(int)index + 2];
                    }
                    if (j - 2 >= 1)
                    {
                        left2 = springlist[(int)index - 2];
                    }
                    if (i + 1 <= taille)
                    {
                        down = springlist[(int)index + (int)taille];
                    }
                    if (i + 2 <= taille)
                    {
                        down2 = springlist[(int)index + (int)taille * 2];
                    }
                    if (i - 2 >= 1)
                    {
                        top2 = springlist[(int)index - (int)taille * 2];
                    }
                
                }
            }
        }

    }

    public Vector3 firstforce(float stifness, Transform elasticpos,float C)
    {
        Vector3 L = elasticpos.transform.position- transform.transform.position ;
        springforce = stifness * (L - (Length  * (L* C / L.magnitude)));
        return springforce;
    }
   
    // Update is called once per frame
    void FixedUpdate()
    {

        dampingforce = damping * vel;
     
        if (top != null && top.gameObject.activeInHierarchy == true)
        {

            SpringForce = firstforce(elacticity, top,1);
        }
        else
        {
          //  Vector3 L =  transform.transform.position;
           // SpringForce += -elacticity * (L - (Length * (L * 1 / L.magnitude)));

        }

     

      if (down != null)
        {
            SpringForce += firstforce(elacticity, down,1);
           

        }
        if (right != null)
        {
            SpringForce += firstforce(elacticity, right,1);
        }
        if (left != null)
        {
            SpringForce += firstforce(elacticity, left,1);
        }
        if (topLeft != null)
        {
            SpringForce += firstforce(elacticity, topLeft,Mathf.Sqrt(2));
        }
        if (topright != null)
        {
            SpringForce += firstforce(elacticity, topright, Mathf.Sqrt(2));
        }
        if (downLeft != null)
        {
            SpringForce += firstforce(elacticity, downLeft, Mathf.Sqrt(2));
        }
        if (downright != null)
        {
            SpringForce += firstforce(elacticity, downright, Mathf.Sqrt(2));
        }

        if (top2 != null)
        {
            SpringForce += firstforce(elacticity, top2,2);
        }
        if (down2 != null)
        {
            SpringForce += firstforce(elacticity, down2,2);
        }
        if (right2 != null)
        {
            SpringForce += firstforce(elacticity, right2,2);
        }
        if( left2 != null)
        {
            SpringForce += firstforce(elacticity, left2,2);
        }

        force = SpringForce + new Vector3(0 ,-g,0) * mass - dampingforce;

        //acceleration=F/M
        acceleration = force / mass;
       
        //  accelerationz = Forcez / mass;

        vel = vel + acceleration * Time.fixedDeltaTime;
      
        //   velz = velz + accelerationz * Time.fixedDeltaTime;

        pos = pos + vel * Time.fixedDeltaTime;
        
        //   Posz = Posz + velz * Time.fixedDeltaTime;

        transform.position =(pos);
    }
}
